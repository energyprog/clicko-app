import vue from 'vue'

import router from 'vue-router'

//importacion de componentes
//componentes de registro de usuarios
import cuentaU from '@/components/usuarios/sesion/pnSesion.vue'

import bienvenido from '@/views/top/topTienda.vue'

import cuentaR from '@/components/usuarios/registro/pnRegistro'

import sesionActiva from '@/components/usuarios/sesionActivo/sesionActiva'

import cuentaRe from '@/components/usuarios/recuperar/pnRecuperar'

import cambiarPss from '@/components/usuarios/recuperar/components/pnUpdateContrasenia'

import shopResum from '@/components/shop/resumen/mainRem'

import shopPublic from '@/components/shop/publicaciones/mainPublic'

import shopEstas from '@/components/shop/estadisticas/estas'

import shopSegClient from '@/components/shop/segCliente/segCliente'

import shopPromos from '@/components/shop/promos/promos'

import cogCuenta from '@/components/config/datos/cuenta'

import error from '@/views/Error'

vue.use(router)

export default new router({

    mode:'history',

    routes:[

        {
            path:'/',
            name:'Welcome',
            component:bienvenido

        },
        //rutas para iniciar sesion usuarios
        {
            path:'/cuenta',
            name:'cuenta',
            component:cuentaU

        },
        //ruta para registro de usuarios
        {
            path:'/cuenta/registro',
            name:'CuentaRegistro',
            component:cuentaR

        },
        //ruta para recuperar cuenta
        {
            path:'/cuenta/recuperar',
            name:'CuentaRecuperar',
            component:cuentaRe
        },
        //ruta para cambiar contraseñia 
        {
            path:'/recuperar-cuenta/:url',
            component:cambiarPss
        },
        //ruta de activasion de cuenta 
        {
            path:'/sesionActiva/:usuario',
            name:'sesionActiva',
            component:sesionActiva
        },
        //ruta para tiendas
        {
            path:'/admin-resumen',
            name:'adminRem',
            component:shopResum
        },
        {
            path:'/admin-publicaciones',
            name:'adminpublic',
            component:shopPublic
        },
        {
            path:'/admin-Estadisticas',
            name:'adminestas',
            component:shopEstas
        },
        {
            path:'/admin-seguimiento',
            name:'adminSeg',
            component:shopSegClient
        },
        {
            path:'/admin-promociones',
            name:'adminpromos',
            component:shopPromos
        },
        //ruta para configuraciones
        {
            path:'/admin-cog-datos',
            name:'cogDatos',
            component:cogCuenta
        },
        //ruta para ERROR 404
        {
            path:'*',
            name:'errores',
            component:error
        }
        
    ]


})