import Vue from 'vue'

import App from './App.vue'

import router from '@/router/main-router'

import GAuth from 'vue-google-oauth2'

import VueGAPI from 'vue-gapi'
 
// create the 'options' object
const apiConfig = {
  apiKey: 'AIzaSyBu0ZR5QAQcB1sHt0MtZvNoy_ziXYY-AlY',
  clientId: '280788420046-2ga9mi79k6ge17bc58sajmpfma63d6lj.apps.googleusercontent.com',
  discoveryDocs: ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
  // see all available scopes here: https://developers.google.com/identity/protocols/googlescopes'
  scope: 'profile email',
  prompt: 'select_account',
  // works only with `ux_mode: "prompt"`
  refreshToken: true,
}

const gauthOption = {
  clientId: '280788420046-2ga9mi79k6ge17bc58sajmpfma63d6lj.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}

Vue.use(GAuth, gauthOption)

// Use the plugin and pass along the configuration
Vue.use(VueGAPI, apiConfig)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
