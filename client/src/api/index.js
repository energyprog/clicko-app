import axios from 'axios'

import cons from '../constantes'

import errores from '../util/error'

let resp;
const dataPeople = async()=>{
    try {
        resp = await axios({
            url:`${cons.url}/vdt/21`,//${cons.user}`,
            method:'GET'
        });
    } catch (error) {
        resp = {data:[[{
            error:false,
            success:true,
            messsage:(await errores.errors.errorExtensTime),
            rows:true
        }],[{
            idShop:0,
            name:'Mi Tienda',
            Rfc:'XXXX93MTXX',
            descrip:'dsjakdasdgsakjdashgdfashgdasgd'
        }]]}
    }
    return resp.data;
}
const modPeople = async(dataPeople)=>{
     resp = await axios({
        url:`${cons.url}//${cons.users}`,
        method:"POST",
        data:{datPeople:dataPeople}
    });
    return resp.data;
}

const addRedes = async(redes)=>{
     resp = await axios({
        url:`${cons.url}/rrs`,
        data:{idShop:cons.users,UrlYou:redes.yous,UrlFace:redes.feces,UrlTw:redes.tws}
    });
    return resp.data;
}

const redesVisual = async() =>{
    try {
        resp = await axios({
            url:`${cons.url}/vrs/${cons.users}`
        });
    } catch (error) {
        resp = {data:[[{
            error:false,
            success:true,
            messsage:(await errores.errors.errorExtensTime),
            rows:true
        }],[{
            idRedes:0,
            You:"",
            Face:"",
            Twitter:""
        }]]}
    }
    return resp.data;
} 

//cog cuenta
const datCuenta = async() =>{
    try{
        resp = await axios({
            url:`${cons.url}/vrs/${cons.users}`
        });
    }catch(error){
        resp = {data:[[{
            error:false,
            success:true,
            messsage:(await errores.errors.errorExtensTime),
            rows:true
        }],[{
            idRedes:0,
            You:"",
            Face:"",
            Twitter:""
        }]]}
    }
    return resp.data
}
export default{
    dataPeople,
    modPeople,
    addRedes,
    redesVisual,
    datCuenta
}