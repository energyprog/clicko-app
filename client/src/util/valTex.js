const expresiones = {
    rfc: /^[A-Z0-18]{18,18}$/gi, // Letras, numeros
    url: /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/gi,
	nombre: /[a-zA-Z]{5,15}/gi, // Letras y espacios, pueden llevar acentos.
	apellidos: /^[a-zA-ZÀ-ÿ\s]{1,40}$/gi, // Letras y espacios, pueden llevar acentos.
	descripcion: /^[a-zA-ZÀ-ÿ\s]{1,40}$/gi, // Letras y espacios, pueden llevar acentos.
	direccion: /^[a-zA-ZÀ-ÿ-9\s]{1,40}$/gi, // Letras,espacios y numeros, pueden llevar acentos.
	//password: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/gi,
	telefono: /^\d{10,13}$/gi // 7 a 14 numeros.
}
const valide = (nameVal, textVal) =>{

    let resp = {error:false,msg:""};
    switch (nameVal) {
        case 'rfc':
            if(expresiones.rfc.test(textVal)){
                resp.error = true;
                resp.msg   = "Escriba correctamente su RFC";
            }
        break;
        case 'name':
            if(expresiones.nombre.test(textVal)){
                resp.error = true;
                resp.msg   = "Escriba correctamente su nombre";
            }
        break;
        case 'lasName':
            if(expresiones.apellidos.test(textVal)){
                resp.error = true;
                resp.msg   = "Escriba correctamente sus apellidos";
            }
        break;
        case 'direct':
            if(expresiones.descripcion.test(textVal)){
                resp.error = true;
                resp.msg   = "Escriba correctamente la descripcion";
            }
        break;
        case 'pass':
            if(expresiones.direccion.test(textVal)){
                resp.error = true;
                resp.msg   = "Escriba correctamente la Dirección";
            }
        break;
        case 'email':
            if(expresiones.correo.test(textVal)){
                resp.error = true;
                resp.msg   = "Escriba correctamente su correo";
            }
        break;
        case 'tel':
            if(expresiones.telefono.test(textVal)){
                resp.error = true;
                resp.msg   = "Escriba correctamente su Telefono";
            }
        break;
        case'url':
            if(expresiones.url.test(textVal)){
                resp.error = true;
                resp.msg = "escriba correcamente el url"
            }
        break;
        default:
            resp.error = true;
            resp.msg = "fuera de contexto"
        break;
    }
    return resp;
}

//creamos funcion para validar cualquier 
//campo de puros caracteres del abecedario
//@Paramas STRING text
const valAlfa = (text)=>{
    let resp = {error:false,msg:""};
    //creamos ciclo for 
    //para envualar letra x letra
    for(let i=0;i<text.length;i++){
        //evaluamos codigo de caracter permitido
        //revisar pagina https://elcodigoascii.com.ar/
        if(text.charCodeAt(i) >= 65  && text.charCodeAt(i) <= 90 || text.charCodeAt(i) >= 97 && text.charCodeAt(i) <= 122 || text.charCodeAt(i) === 32){
            resp.error = false
            resp.msg = false
        }else{
            resp.error = true
            resp.msg = "Ops! Recuerda no escribir caracteres espcieales ni numeros"
        }
    }

    return resp;
}

//creamos funcion para validar cualquier 
//campo de caracteres algunos especiales y alfabeto
//@Paramas STRING text
const valTextCaracter = (text)=>{
    console.log('llamando funcion');
    let resp = {error:false,msg:""};
    //creamos ciclo for 
    //para envualar letra x letra
    for(let i=0;i<text.length;i++){
        //evaluamos codigo de caracter permitido
        //revisar pagina https://elcodigoascii.com.ar/
        if(text.charCodeAt(i) >= 65  && text.charCodeAt(i) <= 90 || text.charCodeAt(i) >= 97 && text.charCodeAt(i) <= 122 || text.charCodeAt(i) >= 48  && text.charCodeAt(i) <= 57){
            resp.error = false
            resp.msg = false
        }else{
            resp.error = true
            resp.msg = "Ops! escribiste caracterer no valido"
        }
    }

    return resp;
}

const valTextNumber = (text)=>{
    let resp = {error:false,msg:""};
    //creamos ciclo for 
    //para envualar letra x letra
    for(let i=0;i<text.length;i++){
        //evaluamos codigo de caracter permitido
        //revisar pagina https://elcodigoascii.com.ar/
        if(text.charCodeAt(i) >= 48  && text.charCodeAt(i) <= 57){
            resp.error = false
            resp.msg = false
        }else{
            resp.error = true
            resp.msg = "Ops! no se admiten letras!"
        }
    }

    return resp;
}

export default {
    valide,
    valAlfa,
    valTextCaracter,
    valTextNumber  
}

