var json_resp = {};

import error from '../error/index';

import api from '../api/index'

const vales = async(email) =>{

    //@trim : limpiar la variable de espacios en blanco
    const limpiarCorreo = email.trim();

    console.log(limpiarCorreo);

    const expresVal =  /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if(expresVal.test(limpiarCorreo) === true){

        json_resp = {

            valError:false,
            mensaje:''

        }

    }else{

        json_resp = {

            valError:true,
            mensaje:(await error.mensaje(0))

        }

    }

    return json_resp;

}

const datosApi = async(email) =>{

    const resp = await api.apiRecuperar(email);

    return resp.data;

}

export default{

    vales,
    datosApi

}