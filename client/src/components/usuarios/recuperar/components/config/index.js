import api from '../api/index'

const valUrl = async(url) =>{

    const resp = await api.sendUrl(url);

    return resp.data;

}

const modPs = async(contra,idU,idT) =>{

    const resp = await api.solidMod(contra,idU,idT);

    return resp.data;
}

export default{

    valUrl,
    modPs

}