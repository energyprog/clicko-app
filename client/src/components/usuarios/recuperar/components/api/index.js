import axios from 'axios'

import dom from '../../../../../constantes';

const sendUrl = async (urls)=>{

    const resp = await axios({

        url:`${dom.url}/recuperaCuenta/${urls}`,
        method:'GET'

    });

    return resp;

}

const solidMod = async(ps,idU,idT) =>{

    const resp = await axios({

        url:`${dom.url}/modContra/${ps}/${idU}/${idT}`,
        method:'GET'

    });

    return  resp;
}

export default{

    sendUrl,
    solidMod

}