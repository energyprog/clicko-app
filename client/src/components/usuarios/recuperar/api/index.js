import axios from 'axios'

import dom from '../../../../constantes';

//var json_resp = {};

const apiRecuperar = async(email) =>{

    const resp = await axios({

        url:`${dom.url}/solidRecuperar/${email}`,
        method:'GET'

    });

    return resp;

}

export default{

    apiRecuperar

}