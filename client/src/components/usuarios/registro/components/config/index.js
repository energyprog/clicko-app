import error from '../error/index'

import api from '../api/index'

var json_mjs = {};

const valInputs = async (nombre,apellidos,correo,pass, confirmpass) =>{

    if(nombre === ""){

        json_mjs = {

            errores:true,
            nomError:true,
            apesError:false,
            correoError:false,
            passError:false,
            conPassError:false,
            mensajeError:(await error.mensajeError(0))
        }

        return json_mjs;

    }else{

        if(apellidos === ""){

            json_mjs = {

                errores:true,
                nomError:false,
                apesError:true,
                correoError:false,
                passError:false,
                conPassError:false,
                mensajeError:(await error.mensajeError(1))
            }

            return json_mjs;

        }else{

            if(correo === ""){

                json_mjs = {

                    errores:true,
                    nomError:false,
                    apesError:false,
                    correoError:true,
                    passError:false,
                    conPassError:false,
                    mensajeError:(await error.mensajeError(2))
                }

                return json_mjs;

            }else{

                if(pass === ""){

                    json_mjs = {

                        errores:true,
                        nomError:false,
                        apesError:false,
                        correoError:false,
                        passError:true,
                        conPassError:false,
                        mensajeError:(await error.mensajeError(3))
                    }

                    return json_mjs;

                }else{

                    if(confirmpass === ""){

                        json_mjs = {

                            errores:true,
                            nomError:false,
                            apesError:false,
                            correoError:false,
                            passError:false,
                            conPassError:true,
                            mensajeError:(await error.mensajeError(4))
                        }
    
                        return json_mjs;

                    }else{

                        json_mjs = {

                            errores:false,
                            nomError:false,
                            apesError:false,
                            correoError:false,
                            passError:false,
                            conPassError:false,
                            mensajeError:'sin errores'
                        }

                        return json_mjs;

                    }

                }

            }

        }

    }

}

const revisionCorreo = async (correo) => {

    const val = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if(val.test(correo) === true){

        json_mjs = {

            error:false,
            errorCorreo:false,
            mensaje:'sin error',

        }

    }else{

        json_mjs = {

            error:true,
            errorCorreo:true,
            mensaje:(await error.mensajeError(5)),

        }

    }

    return json_mjs;

}

const validarCorreo = async(correo) =>{


    const resp = (await api.confirmCorreo(correo)).data;

    return resp;
}

const confirmCode = async(code) =>{

    const reps = await api.CodeConfirm(code);

    return reps.data
}

const envioDatos = async (nombre, apellidos, correo, contrasenia, modal) =>{

    const resp = await api.enviarUsuario(nombre, apellidos, correo, contrasenia, modal);

    return resp.data;
}

const longPass = async(contrasenia) =>{

    var longWidth = contrasenia.length * 6.6;

    if(contrasenia.length > 1 &&  contrasenia.length  <= 5){

        json_mjs = {

            barProgress:true,
            widthProgress:longWidth,
            style:'bg-danger'

        }

    }
    if(contrasenia.length > 5 &&  contrasenia.length  <= 10){

        json_mjs = {

            barProgress:true,
            widthProgress:longWidth,
            style:'bg-warning'

        }

    }
    if(contrasenia.length > 10 &&  contrasenia.length  <= 25){

        json_mjs = {

            barProgress:true,
            widthProgress:longWidth,
            style:'bg-success'

        }

    }
    if(contrasenia.length > 15){

        longWidth = 100;

        json_mjs = {

            barProgress:true,
            widthProgress:longWidth,
            style:'bg-success'

        }

    }

    return json_mjs;

}

const valContra = async(contrasenia, confirm) =>{

    if(contrasenia === confirm){

        json_mjs = {

            error: false,
            mensaje:'correctos'

        }

    }else{

        json_mjs = {

            error: true,
            mensaje:(await error.mensajeError(6))

        }

    }

    return json_mjs;

}
export default {

    validarCorreo,
    envioDatos,
    confirmCode,
    valInputs,
    revisionCorreo,
    longPass,
    valContra

}