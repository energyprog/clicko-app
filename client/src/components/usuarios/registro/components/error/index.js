const error_msj = [

    "debes rellenar el campo nombre",
    "debes rellenar el campo apellidos",
    "debes rellenar el campo correo",
    "debes rellenar el campo contraseña",
    "debes rellenar el campo confirmar contraseña",
    "correo invalido (caracteres invalidos)",
    "contraseñas no coinciden"

]

//var json_mjs = [];

const mensajeError = async (error) =>{

   return error_msj[error];

}
export default{

    mensajeError
}