//fichero para las peticioens 
//al servidor

import axios from 'axios'

import dom from '../../../../../constantes'

//var json_respuesta = {};

const confirmCorreo = async(cor) =>{

  const respuesta = await axios({

        method:'GET',
        url:`${dom.url}/valCorreo/${cor}`

    }).then((resp) =>{

       return resp;

    })

    return respuesta;

}

const CodeConfirm = async(code) =>{

    const respuesta = await axios({

        method:'GET',
        url:`${dom.url}/codeConfirm/${code}`,

    }).then((resp) =>{return resp});

    return respuesta;
}

const enviarUsuario = async(nom, apes, cor, contra, modal) =>{

    const respuesta = axios({

        method:'POST',
        url:`${dom.url}/usuarios/registro`,
        data:{nombre:nom,apellidos:apes,contrasenia:contra,correo:cor, mod:modal}
        
    }).then((resp)=>{

        return resp;

    })

    return respuesta;

}

export default {

    confirmCorreo,
    enviarUsuario,
    CodeConfirm

}