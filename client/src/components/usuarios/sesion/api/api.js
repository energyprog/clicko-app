//archivo con constantes para peticiones al API REST
//funciones con datos, POST, GET, DELETE, para el modulo de registros de usuarios

//exportara las constantes con funciones
//con fin de consumirlas en otro fichero solicitado
import dom from '../../../../constantes';

//importamos libreria de axios
import axios from 'axios'

const datos = {
    'correo':'fpl08999@gmail.como',
    'contrasenia':'crazy123energy!'
}

//funcion para iniciar sesion
const ActivarSesion = async()=>{

    const resp = await axios({

        method:'POST',
        url:'http://192.168.0.7:3100/sesion',
        data:datos

    });

    console.log(resp.data);

    return resp;

}

const sesionG = async(nom,apes,email,pass,modal)=>{

    const respSesion = await axios({

        url:`${dom.url}/activarsesion`,
        method:'POST',
        data:{nombre:nom,apellidos:apes,correo:email,contra:pass,modalidad:modal}

    });

    return respSesion;

}

const sesionNomral = async(correo, contrasenia) =>{

    const resp = await axios({

        url:`${dom.url}/actSesion/${correo}/${contrasenia}`,
        method:'GET',

    });

    return resp;

}


export default{

    ActivarSesion,
    sesionG,
    sesionNomral

}

//>