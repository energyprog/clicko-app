//archivo index con constantes
//funciones del modulo Registro de usuarios

//exportara las constantes con funciones
//con fin de consumirlas en otro fichero solicitado

//importando funciones para la api
import api from '../api/api'

import errores from '../error/index'

const tiposErrores = ['ErrorSen1','ErrorSen2']

var json_return = {};

//verificamos que no haya caracteres 
//no aceptados
const valEmial = async (email) =>{

    //const mensaje = (await api.enviarDatos()).data;

    //console.log(mensaje.mensaje);
    const expresion = /[a-z,0-9]{4,5}@{1,5}/gi;

    if(expresion.test(email) === false){

        //solicitados mensaje de error
        json_return = await errores.correoError(tiposErrores[0]);

    }else{

        json_return = [false,'correo correcto'];

    }
    
    return json_return;

}

//constante incluido con una funcon
//funcion para validar campos @correo y contraseña
const inputsVal = async(emial, contrasenia) =>{

    //validamos campos vacios
    if(emial === "" || contrasenia === ""){

        //solicitamo mensaje de error
       json_return =  await errores.inputsError(tiposErrores[1])

    }else{

        //rellenamos mensaje
        json_return = [false,'puedes proceder']
    }

    return json_return;

}

//funciones para recoger o guardar datos locales
const GuardarSesion = async(id, nombre) =>{

        localStorage.setItem('dis',id)

        localStorage.setItem('ne',nombre)

}


//funcion para mandar datos a la API REST
//recibimos el sello de validacion y ejecutamos funciones
const iniciarSesion = async(sello)=>{

        if(sello === false){

           const resApi = await (api.ActivarSesion())

           json_return = resApi;

        }else{

         //solicitamo mensaje de error
        const sello = await errores.inputsError(tiposErrores[1])

         json_return =  {'sello':[sello[0],sello[1]]}

        }

        return json_return;

}

const googleseion = async(nombre,apellidos,email,pass,modal) =>{

    const resp = await api.sesionG(nombre, apellidos, email,pass,modal);

    return resp.data;

}

const valUsuario = async(correo, contrasenia) =>{

    const resp = await api.sesionNomral(correo, contrasenia);

    return resp.data;

}

const activarSesion = async(nombre,id,sesion) =>{

    const usuario = [id,nombre,sesion];

    localStorage.setItem('usuarioActivo',JSON.stringify(usuario));

    return true;

}

export default{

    valEmial,
    inputsVal,
    iniciarSesion,
    GuardarSesion,
    googleseion,
    activarSesion,
    valUsuario
    
}