//fichero para errores
//finalizadad de imprimir mensajes de errores
//para modulo de sesiones

//objeto con mensajes para tipos de errores
const object_errores = {

    'ErrorSen1':[true,'correo invalido'],
    'ErrorSen2':[true,'rellena correctamente lo campos solicitados']

};

//retorna mensaje para el error solicitado
const correoError = async(error) =>{

    return object_errores[error]

}

//retorna mensaje para el error solicitado
const inputsError = async(error) =>{

    return object_errores[error]

}

export default{

    correoError,
    inputsError

}