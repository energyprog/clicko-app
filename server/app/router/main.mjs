//requirimos libreria de las rutas
import pkg from 'express';
const { Router } = pkg;
export const routers = Router();

//uso de token de sesion
import jwt from 'jsonwebtoken';
//controllers of account
import {sesionAct,recoveryAccount,sesionsGoogle,updatepass,valyrecoveryUrl} from '../controller/users/account.mjs';
//controllers of register
import {registry,createCodes,confirmCodes,valeamil} from '../controller/users/registry.mjs';
//controladres de seguimiento 
import {printTrancking} from '../controller/product/productTrancking.mjs';
//contraldor para siguimiento de conversaciones
import { trackingChat } from '../controller/chat/trackingClient.mjs';
//contraolador para productos no empaquetados
import { orderNotdone } from '../controller/product/orderNotdone.mjs';
//rutas generales de POST
//reouter registry user
routers.post('/usuarios/registro',registry);
//router create code for account
routers.post('/cuenta/code',createCodes);
//rutas generales de GET

//router vality email
routers.get('/valCorreo/:correo',valeamil);
//router confirmer code
routers.get('/codeConfirm/:code',confirmCodes);
//router solid recovery accounts
routers.get('/solidRecuperar/:correo',recoveryAccount);
//router url recovery valality
routers.get('/recuperaCuenta/:url',valyrecoveryUrl);
//router update password 
routers.get('/modContra/:contrasenia/:ids/:idUrls',updatepass);
//router active sesion google
routers.post('/activarsesion',sesionsGoogle);
//router actve sesion normal
routers.get('/actSesion/:correo/:pass',sesionAct);
//router para seguimiento de producto en tienda
routers.get('/productTracking/:idShop/:status',printTrancking);
//ruta para siguimiento de conversación para tiendas
routers.get('/chatTracking/:idShop',trackingChat);
//ruta para productos no empaquetados
routers.get('/orderNotdonde/:idShop',orderNotdone);