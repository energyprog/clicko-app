//fichero para controladores de redes sociales
//importamos modelo de base de datos de red sociales
import { redSocial } from '../../model/redSocial.mjs';
import { ErrorRedSoccial } from '../../util/errors.mjs';
import { successRedSocial } from '../../util/sucess.mjs';

export const registerSoccial = async (req, res)=>{
    let response = [];
    let data = [];
    let message = [];
    //recogemos los datos 
    const {idShop,UrlYou,UrlFace,UrlTw} = req.body;

    //validamos el registro
    if((await redSocial.findOne({where:{IdTienda:idShop}})) === null){
        try{
            const newRedSocial = await redSocial.create({
                linkFace:UrlFace,
                LinkYou:UrlYou,
                LinkTw:UrlTw,
                IdTienda:idShop
            })
            if(newRedSocial){
                message.push({
                    error:false,
                    succesS:true,
                    panel:true,
                    message:(await successRedSocial.success_001)
                })
            }else{
                message.push({
                    error:true,
                    succesS:false,
                    panel:true,
                    message:(await ErrorRedSoccial.errorRed_02)
                })
            }
        }catch(e){
            console.log(e);
            message.push({
                error:true,
                succesS:false,
                panel:true,
                message:(await ErrorRedSoccial.errorRed_02)
            })
        }
    }else{
        message.push({
            error:true,
            succesS:false,
            panel:true,
            message:(await ErrorRedSoccial.errorRed_01)
        })
    }

    setTimeout(()=>{
        response.push(message,data);
        res.json(response);
    },1500)
}

export const visualizeRedes = async (req, res) =>{
    let response = [];
    let data = [];
    let message = [];

    const { idShop } = req.params;
    const ShowRed = await redSocial.findOne({where:{IdTienda:idShop}});
    if(ShowRed != null){
        message.push({
            error:false,
            succesS:true,
            panel:true,
            message:''
        })
        data.push({
            You:(ShowRed.dataValues.LinkYou).trim(),
            Face:(ShowRed.dataValues.linkFace).trim(),
            Twitter:(ShowRed.dataValues.LinkTw).trim(),
            idRedes:ShowRed.dataValues.IdRed
        });
    }else{
        message.push({
            error:true,
            succesS:false,
            panel:true,
            message:(await ErrorRedSoccial.errorRed_03)
        })
    }

    setTimeout(()=>{
        response.push(message,data);
        res.json(response);
    })
}

export const deleteRed = async (req, res) =>{
    let response = [];
    let data = [];
    let message = [];

    const { idRedes } = req.params;
    
    if((await redSocial.findOne({where:{IdRed:idRedes}})) != null){
        try{
            redSocial.destroy({
                where:{IdRed:idRedes}
            })
            message.push({
                error:false,
                success:true,
                panel:true,
                message:(await successRedSocial.success_002)
            })
        }catch(e){
            message.push({
                error:true,
                succesS:false,
                panel:true,
                message:(await ErrorRedSoccial.errorRed_02)
            })
        }        
    }else{
        message.push({
            error:true,
            succesS:false,
            panel:true,
            message:(await ErrorRedSoccial.errorRed_03)
        })
    }
    setTimeout(()=>{
        response.push(message)
        res.json(response)
    },1500)
}

export const updateRed = async (req, res) =>{
    let response = [];
    let data = [];
    let message = [];

    const { idRedes,urlYou,urlFace,urlTw } = req.body;

    const red = await redSocial.findOne({
        where:{
            IdRed:idRedes
        }
    });
    try{
        console.log(red);
        if(red != null){
            red.dataValues.forEach(async redes=>{
                await redes.update({
                    linkFace:urlFace,
                    LinkYou:urlYou,
                    LinkTw:urlTw,
                })
            })
            setTimeout(()=>{
                console.log(red);
            },1500)
        }
    }catch(e){
        console.log(e);
    }
}