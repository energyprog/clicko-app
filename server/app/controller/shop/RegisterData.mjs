//contralador para registrar los datos faltantes de tienda
//function para actualizar datos faltantes de tienda

import { shop } from '../../model/ShopClickO.mjs';
import { user } from '../../model/userClickO.mjs';
import { ErroShop } from '../../util/errors.mjs';
import { successRedSocial } from '../../util/sucess.mjs';
import { dateDeletShop,gettimeDate } from '../../util/tiempo/index.mjs';
import { valide } from '../../util/approveText.mjs'

export const registerShop = async(req, res)=>{

    let respController = [];
    let data = [];
    let dataShop = [];
    const {IdUser, nameShop, descriptionShop, rfcShop } = req.body;

    const valName = await valide(nameShop);
    const valDescript = await valide(descriptionShop);
    const valRfc = await valide(rfcShop);

    setTimeout(()=>{
      //  if(valName !=)
    },1000)

    if((await user.findOne({where:{IdUsuario:IdUser}})) != null){
        try{
            let x = 0;
            const shops = await shop.findAll();
            shops.forEach(async shopies =>{
                    if(((await shopies.dataValues.NombreTienda).trim()) === nameShop){
                        x = 1;
                    }
            })
            setTimeout(async()=>{
                if(x === 0){
                    try{
                        const NewShop = await shop.create({
                            IdUsuario:IdUser,
                            NombreTienda:nameShop,
                            DescripcionTienda:descriptionShop,
                            RfcTienda:rfcShop,
                        })
                        if(NewShop){
                            data.push({
                                menssage:'Registro realizado correctamente!',
                                error:false,
                                success:true,
                                process:true
                            })
                            dataShop.push({
                                id:NewShop.IdTienda,
                                name:NewShop.NombreTienda
                            })
                        }
                    }catch(e){
                        data.push({
                            menssage:(await ErroShop.errorShop_03),
                            error:true,
                            success:false,
                            process:true
                        })
                    }
                }else{
                    data.push({
                        menssage:(await ErroShop.errorShop_01),
                        error:true,
                        success:false,
                        process:true
                    })
                }
            },100)
        }catch(e){
            data.push({
                menssage:(await ErroShop.errorShop_03),
                error:true,
                success:false,
                process:true
            })
        }
    }else{
        data.push({
            menssage:(await ErroShop.errorShop_03),
            error:true,
            success:false,
            process:true
        })
    }
    
    setTimeout(()=>{
        respController.push(data);
        res.json(respController)
    },1500)
}

export const selectShop = async(req, res)=>{
    const {idShop} = req.params;

    let response = [];
    let data     = [];
    let messsage = [];

    try{
        const Storie = await shop.findOne({
            where:{
                IdTienda:idShop
            }
        });
        if(Storie === null){
            messsage.push({
                error:true,
                success:false,
                panel:true,
                messsage:(await ErroShop.errorShop_02)
            })
        }else{
            messsage.push({
                error:false,
                success:true,
                panel:true,
                messsage:''
            })
            data.push({
                idShop:(Storie.dataValues.IdTienda).trim(),
                name:(Storie.dataValues.NombreTienda).trim(),
                Rfc:(Storie.dataValues.RfcTienda).trim(),
                descrip:(Storie.dataValues.DescripcionTienda).trim()
            })
        }
        console.log(Storie);
    }catch(e){
        console.log(e);
        messsage.push({
            error:true,
            success:false,
            panel:true,
            messsage:(await ErroShop.errorShop_03)
        })
    }

    setTimeout(()=>{
        response.push(messsage,data);
        res.json(response);
    },1000)
}

let dataDelete = 0;
export const deletShop = async(req, res)=>{
    const {idShop} = req.params;

    let response = [];
    let data     = [];
    let messsage = [];

    if((await shop.findOne({where:{IdTienda:idShop}})) != null){
        if(dataDelete >= (await gettimeDate)){

        }else{
            try{
                shop.destroy({
                    where:{IdTienda:idShop}
                })
                messsage.push({
                    error:false,
                    success:true,
                    panel:true,
                    messsage:(await successRedSocial.success_002)
                })
                dataDelete = await dateDeletShop;
            }catch(e){
                messsage.push({
                    error:true,
                    success:false,
                    panel:true,
                    messsage:(await ErroShop.errorShop_03)
                })
            }
        }
    }

    setTimeout(()=>{
        response.push(messsage);
        res.json(response);
    })
}