//controlador para productos no preparados
import { product } from "../../model/products.mjs";
import { Buysell } from "../../model/Buysell.mjs";
import { client } from "../../model/clientClickO.mjs";
import { productTracking } from '../../model/ProductTracking.mjs';
import { user } from '../../model/userClickO.mjs';

export const orderNotdone = async (req, res) =>{

    let datOrder = [];
    let data = {};
    let order = [];
    let num = 0;

    const { idShop } = req.params;

    try{
        const trancking = await productTracking.findAll({
            where:{
                EstadoProceso:1
            }
        });
        trancking.forEach(async tranckin => {
            const sell = await Buysell.findAll({
                where:{
                    IdCV:tranckin.CompraVentumIdCV,
                    IdTienda:idShop
                }
            });
            const datClient = await client.findAll({
                where:{
                    IdCliente:sell[0].dataValues.IdCliente
                }
            });
            const datUser = await user.findAll({
                where:{
                    IdUsuario:datClient[0].dataValues.IdUsuarios
                }
            });
            const datProduct = await product.findAll({
                where:{
                    IdProducto:sell[0].dataValues.IdProducto
                }
            });
            order.push({
                nameUser:((datUser[0].dataValues.NombreUsuario)).trim(),
                nameProduct:(datProduct[0].dataValues.NombreProducto).trim(),
                quantity:1,//sell[0].dataValues.cantidad,
                datSell:sell[0].dataValues.FechaCV,
            });
        });
        data = {
            message:'',
            error:false,
            success:true
        }
    }catch(e){
        //console.log(e);
        data = {
            message:'Algo salio mal!',
            error:true,
            success:false
        };
    }
    num++;
    setTimeout(()=>{
        datOrder.push(data,order,num);
        res.json(datOrder);
    },1000)

}