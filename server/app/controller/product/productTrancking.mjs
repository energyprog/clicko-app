//controlador para el seguimiento del productos
//importamos api seguimeinto producto
import { productTracking } from '../../model/ProductTracking.mjs';
import { Buysell } from '../../model/Buysell.mjs';

Buysell.hasMany(productTracking,{foreinkey:'IdCV'});

export const printTrancking = async(req, res)=>{
    const { idShop,status } = req.params;

    let  prodTracking = [];
    let  data = [];
    //buscamos id compra venta y id del producto
    try{
        const Buy = await Buysell.findAll({
            where:{
                IdTienda:idShop,
            },
            include:productTracking
        });
        Buy.forEach(async element => {
            if(element.dataValues.rastreoProductos[0].dataValues.EstadoProceso === parseInt(status)){
                prodTracking = [{
                    error:false,
                    success:true,
                    message:""
                }];
                data.push(element.dataValues.rastreoProductos[0].dataValues);   
            }else{
                prodTracking = [{
                    error:false,
                    success:true,
                    message:"no existen datos"
                }];
            }
        });
    }catch(e){
        return {
            error:true,
            success:false,
            message:'Algo salio mal!'
        }
    }
    setTimeout(() => {
        prodTracking.push(data);
        res.json(prodTracking);
    }, 100);
}