//fichero para siguimiento de conversión con tienda
import { chat } from "../../model/chat.mjs";
import { client } from '../../model/clientClickO.mjs';
import { user } from '../../model/userClickO.mjs';
//import { user } from '../../model/userClickO.mjs';

//shop.hasMany(chat,{foreinkey:'IdTienda'});
//user.hasMany(client,{foreinkey:'IdUsuario'});

export const trackingChat = async (req, res) =>{
    const { idShop } = req.params;
    let tranckChat = [];
    let data = {};
    let datChat = [];
    //buscamos id y estado del chat
    try{
        const ChatWithShop =  await chat.findAll({
            where:{
                IdTiendas:idShop,
            }
        });
        ChatWithShop.forEach(async speaking =>{
            const datClient = await client.findAll({
                where:{
                    IdCliente:speaking.IdClientes
                }
            });
            const datUser = await user.findAll({
                where:{
                    IdUsuario:datClient[0].dataValues.IdUsuarios
                }
            });
            datChat.push({
                idChat:speaking.IdConversacion,
                createAt:speaking.createdAt,
                nameClient:(((datUser[0].dataValues.NombreUsuario).split(" "))[0]).trim(),
                message:(((speaking.mensajes).split(" "))[0]).trim(),
            });
        })
        data = {
            error:false,
            success:true,
            message:""
        };
        tranckChat.push(data,datChat);
    }catch(e){
        console.log(e);
        data = [{
            error:false,
            success:true,
            message:"Algo salio mal!"
        }];
        tranckChat.push(data)
    }
    setTimeout(()=>{
        res.json(tranckChat);
    },1000)
}