import { consolaRequest } from '../../config/index.mjs';
//-----------------------------------------------------------
import { Regisros } from '../../api/usuarios/registro/index.mjs';

export const registry = async (req, res) =>{
    consolaRequest(req);
    
    datos =  req.body;
    respuesta = await Regisros(datos);
    console.log(respuesta);

    res.json(respuesta);
}
//-----------------------------------------------------------
import { valCorreo } from '../../api/usuarios/registro/index.mjs';

export const valeamil = async(req, res)=>{
    consolaRequest(req);

    const correo = req.params.correo;
    const resp = await valCorreo(correo);
    res.json(resp);

}
//-------------------------------------------------------------
import { createCode } from '../../api/usuarios/registro/index.mjs';

export const createCodes = async (req, res) =>{
    consolaRequest(req);

    const datos = req.body;
    const respe = await createCode(datos);

    res.json(respe);
 
}
//------------------------------------------------------------
import { confirmCode } from '../../api/usuarios/registro/index.mjs'

export const confirmCodes = async(req, res)=>{
    consolaRequest(req);

    datos = req.params.code;
    respuesta = await confirmCode(datos);
    res.json(respuesta);

}
