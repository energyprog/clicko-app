//modelo para un chat entre cliente ClickO
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const product = conectdb.define('Productos',{
    IdProducto:{
        type:Sequalize.INTEGER,
        primaryKey:true
    },
    IdTienda:{
        type:Sequalize.INTEGER
    },
    NombreProducto:{
        type:Sequalize.STRING
    },
    Existencia:{
        type:Sequalize.INTEGER
    },
    Disponibilidad:{
        type:Sequalize.INTEGER
    },
    PesoGramos:{
        type:Sequalize.INTEGER
    },
    Calidad:{
        type:Sequalize.INTEGER
    },
    AniosMinimo:{
        type:Sequalize.INTEGER
    },
    TipoEnvio:{
        type:Sequalize.INTEGER
    },
    DescripcionProducto:{
        type:Sequalize.STRING
    }
});