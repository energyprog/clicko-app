//modelo para un chat entre cliente y comprador
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const chat = conectdb.define('conversacion',{
    IdConversacion:{
        type:Sequalize.INTEGER,
        primaryKey:true
    },
    IdClientes:{
        type:Sequalize.INTEGER
    },
    IdTiendas:{
        type:Sequalize.INTEGER
    },
    mensajes:{
        type:Sequalize.STRING
    },
    EstadoChat:{
        type:Sequalize.INTEGER
    }
});