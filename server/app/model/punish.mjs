//modelo de rastreo de productos
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const punish = conectdb.define('Castigos',{
    idCastigo:{
        type:Sequalize.INTEGER,
        primaryKey:true,
        autoIncrement: true,
    },
    nombreCastigo:{
        type:Sequalize.STRING
    },
    descCastigo:{
        type:Sequalize.STRING
    }
});

