//modelo para un chat entre cliente ClickO
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const client = conectdb.define('clienteClicko',{
    IdCliente:{
        type:Sequalize.INTEGER,
        primaryKey:true
    },
    IdUsuarios:{
        type:Sequalize.INTEGER
    }
});