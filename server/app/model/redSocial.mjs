//modelo para tabla de redes sociales
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const redSocial = conectdb.define('redsociales',{
    IdRed:{
        type:Sequalize.INTEGER,
        primaryKey:true,
        autoIncrement: true,
    },
    linkFace:{
        type:Sequalize.STRING
    },
    LinkYou:{
        type:Sequalize.STRING
    },
    LinkTw:{
        type:Sequalize.STRING
    },
    IdTienda:{
        type:Sequalize.INTEGER
    }
});