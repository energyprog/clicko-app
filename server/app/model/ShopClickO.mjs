//modelo para Tienda ClickO
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const shop = conectdb.define('tiendaClicko',{
    IdTienda:{
        type:Sequalize.INTEGER,
        primaryKey:true
    },
    IdUsuario:{
        type:Sequalize.INTEGER
    },
    NombreTienda:{
        type:Sequalize.STRING
    },
    DescripcionTienda:{
        type:Sequalize.STRING
    },
    RfcTienda:{
        type:Sequalize.STRING
    }
});