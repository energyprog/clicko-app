//modelo de compra y venta
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const textInvalide = conectdb.define('palabInvalidas',{
    idPalabra:{
        type:Sequalize.INTEGER,
        primaryKey:true,
        autoIncrement: true,
    },
    palabra:{
        type:Sequalize.STRING
    }
});