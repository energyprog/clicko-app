//modelo de compra y venta
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const Buysell = conectdb.define('CompraVenta',{
    IdCV:{
        type:Sequalize.INTEGER,
        primaryKey:true
    },
    IdCliente:{
        type:Sequalize.INTEGER
    },
    IdTienda:{
        type:Sequalize.INTEGER
    },
    IdProducto:{
        type:Sequalize.INTEGER
    },
    cantidad:{
        type:Sequalize.INTEGER
    },
    FechaCV:{
        type:Sequalize.DATE
    }
});