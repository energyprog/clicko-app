//modelo de rastreo de productos
import Sequalize from 'sequelize';
import { conectdb } from '../db/database.mjs';

export const productTracking = conectdb.define('rastreoProductos',{
    IdRastreo:{
        type:Sequalize.INTEGER,
        primaryKey:true
    },
    CompraVentumIdCV:{
        type:Sequalize.INTEGER
    },
    EstadoProceso:{
        type:Sequalize.INTEGER
    }
});

