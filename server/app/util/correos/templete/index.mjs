//fichero con las configuraciones para el envio del correo

var view = "";

//requirmos fichero con control de fechas
import {fechaHoy,horaHoy} from '../../tiempo/index.mjs';

export const credenciales = async()=>{

    const credencial = {

        host:"smtp.gmail.com",
        port:587,
        auth:{
            user:"Clickoecommerce@gmail.com",
            pass:"ClickO00!"
        }
     }

     return credencial;

}

export const optionesEnvio = async(usuario,correo,titulo,vista) =>{

    const opciones = {

        from:usuario,
        to:correo,
        subject:titulo,
        html:vista
    }

    return opciones;

}

//@PARAMETROS nombre STRING
//@RETURN view STRING
export const recuperarCuenta = async(nombre, link)=>{

    view = `
    <div style='width:75%;margin:auto;' >
    <div style='width:100%;height:50px;background-color:#00B4AD;margin-bottom:15px;'></div>
    <div style='width:90%;margin-left:auto;margin-right:auto;' >
        <div style='float:left;margin-bottom:15px'>
            <b style='font-size:15px;' >${(await fechaHoy())}</b>
        </div>
        <div style='float:right;margin-bottom:15px;' >
            <b style='font-size:15px;' >${(await horaHoy())} horas</b>
        </div>
    <div style='width:100%;text-align:center;padding-top: 35px;' >
        <span style='font-size:15px;' >
        Hola ${nombre} se ha solicitado tu cambio de contraseña en ClickO da click en el link que aparece a continuación durante los próximos 5 minutos para crear una nueva. 
        </span>
            <br>
        <b style='font-size:55px;color:#F20B00;' >${link}</b>
        <br>
        <span style='font-size:15px;' >Se cambio la contraseña de tu cuenta de ClicKO.  Si no fuiste tú <a href='http://192.168.0.4/cuenta/recuperar'>recupera tu cuenta.</a></span>  
    </div>
    </div>
    <div style='width:100%;height:50px;background-color:#00B4AD;margin-top:15px;' ></div>
</div>
    `;

    return view;
}