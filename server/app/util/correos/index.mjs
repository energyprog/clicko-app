//fichero para envio de correos a los usuarios

//requerimos lirberia de correos
import pgk from 'nodemailer';

const email = pgk;

//requrimos fichero index
import {credenciales,recuperarCuenta,optionesEnvio} from './templete/index.mjs';

let titulo = "";
let viewCorreo = "";
let optionesCorreo;
let credencial;
//variable global
var json_respuesta = {};

//funcion para enviar mensajes

//funcion de recuperacion de cuenta
//@parametro correo STRING
//@parametro nombre STRING
export const correoRecuperarCuenta =async (correo, nombre, link, motivo)=>{

    titulo = motivo;

    credencial = await credenciales();

    viewCorreo = await recuperarCuenta(nombre,link);

    optionesCorreo = await optionesEnvio(nombre,correo,titulo,viewCorreo);

    email.createTransport(credencial).sendMail(optionesCorreo);

    json_respuesta = {

        correo:true

    }

    return json_respuesta;

}
