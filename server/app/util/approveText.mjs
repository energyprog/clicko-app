//fichero para validar texto enviado desde el cliente
//con el fin de validar palabras inapropiadas 
import { textInvalide } from '../model/textInvalide.mjs';
//import {} from './errors.mjs';

export const valide = async(txt)=>{
    const TextInval = await textInvalide.findAll();
    let expresVal =  false;
    TextInval.forEach(async text =>{
            const expre = new RegExp(`${(text.palabra).trim()}`,'i');
            if(expre.test(txt)){
                expresVal = true;
            }
    });

    return expresVal;
}
