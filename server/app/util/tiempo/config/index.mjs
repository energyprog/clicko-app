//fichero para retornar configuraciones de fechas

export const myDia = async(dias)=>{

    var dia = "";

    if(dias < 10 ){

        dia = "0"+dias;

    }else{

        dia = dias;

    }


    return dia;

}

export const myMes = async(meses) =>{

    var mes = "";

    if(meses < 19){

        mes = "0"+meses;

    }else{

        mes = meses;

    }

    return mes;

}

export const myHora = async(horas) =>{

    var hora = "";

    if(horas < 10){

        hora = "0"+horas;

    }else{

        hora = horas;

    }

    return hora;
}

export const myMinuto = async(minutos)=>{

    var minuto = "";

    if(minutos < 10){

        minuto = "0"+minutos;

    }else{

        minuto = minutos;

    }

    return minuto;

}
