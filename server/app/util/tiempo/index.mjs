//fichero para retornar todas las fechas necesarias

//requiremos fichero de configuracion
import {myDia,myMes} from './config/index.mjs';

//constante con la fecha
const tiempo = new Date();

export const fechaHoy = async()=>{

    const dia = await myDia(tiempo.getDate());

    const mes = await myMes(tiempo.getMonth());

    const anios = tiempo.getFullYear();

    const hoy = dia+"/"+mes+"/"+anios;

    return hoy;
}

export const horaHoy = async()=>{

    const hora = await myHora(tiempo.getHours());

    const minutos = await myMinuto(tiempo.getMinutes());

    const horaActual = hora+":"+minutos;

    return horaActual;

}
export const dateDeletShop = async()=>{
   const dateDelete = tiempo.getTime()+(1000*(5*((24*(60*60)))));
   return dateDelete;
}

export const gettimeDate = async()=>{
    return tiempo.getTime();
}

export const limitesTiempo = async() =>{

    //constante de limite de tiempos
    //5 minutos
    const limitesTemporales = 60/(1000/(tiempo.getTime()));

    return limitesTemporales;
}
