//requerimos librerias
import http from 'express';

const app = http();

import cors from 'cors';

//import morgan from 'morgan';

const port = 3100;

//middeware
app.use(cors());

//app.use(morgan('dev'));

app.use(http.json());

app.use(http.urlencoded({extended: true}));

import {routers} from './router/main.mjs';

//require todas las rutas
app.use(routers);

//ejecucion del servidor
//usa puerto
app.listen(port,()=>{

    console.log('server start on 192.168.0.3:'+port);

});




