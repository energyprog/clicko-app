export const consolaRequest = (req, res, next) =>{

    const objPeticion = {

        ip:req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        urlOrgin:req.originalUrl,
        body:req.body,
        parametros:req.params
    }

    console.table([objPeticion]);
  
}