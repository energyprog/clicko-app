//fichero con todas las consultas necesarias para la base de datos
//requirmos fichero de conexion con base de datos
import { connectdb } from '../../../../db/connectdb.mjs';

import {fechaHoy,limitesTiempo} from '../../../../util/tiempo/index.mjs';

//requirmos fichero de configuracion
import {correoCode} from '../config/index.mjs';
//const { compareSync } = require('bcrypt');
//const error = require('../error/index')
const db = connectdb();
var data = [];
var json_resp;

export const queryRegistro = async (datos) =>{
    const hoy = await fechaHoy();
    data = [
        datos.nombre,
        datos.apellidos,
        null,
        datos.correo,
        datos.contrasenia,
        hoy,
        null
    ];
    try{
        const consulta = 'INSERT INTO "usuariosClicko"("NombreUsuario", "ApellidosUsuarios", "FechNacimiento", "CorreoUsuario", contrasenia, "FechaRegistro", "FechaActualizar") VALUES($1,$2,$3,$4,$5,$6,$7)';
        const resp = await db.query(consulta,data);
        const consultaB = 'SELECT "IdUsuario" FROM public."usuariosClicko" WHERE "CorreoUsuario" = $1';
        const datosb = [datos.correo];
        const respConsutla = await db.query(consultaB,datosb);
        if(resp.rowCount === 1){
            if(datos.mod === 1){
                queryRegistroCliente(respConsutla.rows[0].IdUsuario);
                json_resp = {
                    registro:true,
                }
            }
            json_resp = {

                registro:true,

            }

        }
    }catch(e){
       json_resp = {
            registro:false
       }
    }
    return json_resp;
}
export const queryRegistroCliente = async(idUser) =>{
    const consulta = 'INSERT INTO public."clienteClicko"("IdUsuarios", "FechaRegistro", "FechaActualizar") VALUES ($1, $2, $3)';
    const datos = [(await idUser),(await fechaHoy()),null];
    const registro = db.query(consulta,datos);
}

export const queryCode = async(correo) =>{
        const abcRequerido = "123abcd456efg789hij0zyxwvutsRQPKLMNO";
        const correos = await correo;
        var code = "";
        for(let i = 0;i<14;i++){
            code = code+abcRequerido[Math.floor(Math.random() * (abcRequerido.length - 0))];
        }
        const hoy = await fechaHoy();
        const time = new Date();
        const tiempo = 60/(1000/((time.getTime())+1000*(60*5)));
        const consulta = 'INSERT INTO "codeConfirm"("CodigoConfirm", "tiempoLimite", "EstadoCodigo", "fechaRegistro", "fechaActualizar") VALUES($1,$2,$3,$4,$5)';
        data = [
            code,
            tiempo,
            1,
            hoy,
            null
        ]
        const insertar = await db.query(consulta, data);
        if(insertar.rowCount === 1){
            correoCode(correos,code);
            json_resp = {
                correo:[
                    {
                        error:false,
                        success:true,
                        mensaje:"correcto"
                    }
                ]
            }
        }
        console.log(json_resp);
        return json_resp;
}

export const querySearchCode = async(dato) =>{
   const codes = [dato,1];
    //const time = new Date();
    //const tiempo = 60/(1000/(time.getTime()));//time.getHours()+":"+time.getMinutes();
    const consulta = 'SELECT "idConfirm", "tiempoLimite", "EstadoCodigo" FROM public."codeConfirm" WHERE "CodigoConfirm" = $1 AND "EstadoCodigo" = $2';
    const atributosCode = await db.query(consulta, codes);
    if(atributosCode.rowCount === 1){
    const idConfirm = atributosCode.rows[0].idConfirm;
    const timeLimite = atributosCode.rows[0].tiempoLimite;
    const estadoCodes = atributosCode.rows[0].EstadoCodigo;
    if((await limitesTiempo()) > timeLimite){
        modCodes(2,idConfirm);
        json_resp = {
            confirmCode:false,
            mensaje: (await error.mensaje(0))
        }
    }else{
        if(estadoCodes > 1){
            json_resp = {
                confirmCode:false,
                mensaje:(await error.mensaje(0))
            }
        }else{
            json_resp = {
                confirmCode:true,
                mensaje:'sin errores'
            }
        }
    }
    }else{
        json_resp = {
            confirmCode:false,
            mensaje:(await error.mensaje(0))
        }
    }
    return json_resp;
}

export const querUsuario = async(dato) =>{
    const datos = [await dato];
    const consulta = 'SELECT * FROM public."usuariosClicko" WHERE "CorreoUsuario" = $1';
    try{
        const mapeo = await db.query(consulta,datos);
        if(mapeo.rowCount > 0){
            json_resp = {
                error:false,
                ids:mapeo.rows[0].IdUsuario,
                nombres:mapeo.rows[0].NombreUsuario,
                Apellidos:mapeo.rows[0].ApellidosUsuarios,
                Nacimiento:mapeo.rows[0].FechNacimiento,
                Correo:mapeo.rows[0].CorreoUsuario,
                Contrasenia:mapeo.rows[0].contrasenia,
                FechaRegistro:mapeo.rows[0].FechaRegistro,
                FechaActualizar:mapeo.rows[0].FechaActualizar
            }
        }else{
            json_resp = {
                error:true
            }
        }
    }catch(e){
        json_resp = {
            error:true
        }
    }
  return json_resp;
}
export const modCodes = async(estatus,id) =>{
    const nuevoStado = await estatus;
    const consulta = 'UPDATE public."codeConfirm" SET "EstadoCodigo" = $1, "fechaActualizar" = $2, WHERE "idConfirm" = $3';
    const datos = [nuevoStado,(await fechaHoy()),id];
}