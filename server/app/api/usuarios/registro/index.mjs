//fichero para realizar consultas 
//En este caso: registro de usuarios
var data = [];

//requerimos funciones de db
import {querySearchCode,queryCode} from './db/index.mjs';

var json_resp = {};

export const valCorreo = async(correo) =>{
    const mepeos = await dbr.querUsuario(correo);
    if(mepeos.error === true){
        const registrarCode = await queryCode(correo);
        return registrarCode;
    }else{
        json_resp = {
            correo:[
                {
                    error:true,
                    success:false,
                    mensaje:"Ya hay un registro con el mismo correo"
                }
            ]
        }
        return json_resp;
    }

}
export const confirmCode = async (datos) =>{
    const code = datos;
    const resp = await querySearchCode(code);
    return resp;

}
export const createCode = async(datos) =>{
   const resp = await queryCode(datos);
   return resp;

}
export const Regisros = async (datos) =>{
    const resp = await dbr.queryRegistro(datos);
        sesion.activarSesion(datos.correo,1);
        console.log(resp);
        return resp;
}