export const viewCodigo = (nombre, code, hora, fecha) =>{

    const vista = `
    <div style='width:75%;margin:auto;' >
    <div style='width:100%;height:50px;background-color:#00B4AD;margin-bottom:15px;'></div>
    <div style='width:90%;margin-left:auto;margin-right:auto;' >
        <div style='float:left;margin-bottom:15px'>
            <b style='font-size:15px;' >${fecha}</b>
        </div>
        <div style='float:right;margin-bottom:15px;' >
            <b style='font-size:15px;' >${hora} horas</b>
        </div>
    <div style='width:100%;text-align:center;padding-top: 35px;' >
        <span style='font-size:15px;' >Hola ${nombre} te mandamos tu código de verificación, recuerda que tienes únicamente 5 minutos para poder confirmar tu correo con el siguiente código:</span>
            <br>
        <b style='font-size:55px;color:#F20B00;' >${code}</b>
    </div>
    </div>
    <div style='width:100%;height:50px;background-color:#00B4AD;margin-top:15px;' ></div>
</div>
    `;

    return vista;

}