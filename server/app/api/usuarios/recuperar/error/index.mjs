const mensajeError = [

    'No existen registro del correo solicitado',
    'en tiempo anterior se a enviado un correo para recuperar tu cuenta',
    'No existe la página solicitada o supero el tiempo limite de actividad',
];

export const mensaje = async (error) =>{

    return mensajeError[error];

}
