//fichero con consultas de base de datos
//para recuperacion de cuenta
import { connectdb } from '../../../../db/connectdb.mjs';

//requirmos fichero para control de tiempo
import {fechaHoy} from '../../../../util/tiempo/index.mjs';

//requerimos fichero con mensajes de error
import {mensaje} from '../error/index.mjs';

const db = connectdb();

var json_respuesta = {};

var time = new Date();

export const usuarioCorreo = async (correo) =>{

    try{

        const consulta = 'SELECT * FROM public."usuariosClicko" WHERE "CorreoUsuario" = $1';
        
        const dato = [correo];

        const mapeo = await db.query(consulta, dato);

        json_respuesta = {

            error:false,
            ids:mapeo.rows[0].IdUsuario,
            nombres:mapeo.rows[0].NombreUsuario,
            Apellidos:mapeo.rows[0].ApellidosUsuarios,
            Nacimiento:mapeo.rows[0].FechNacimiento,
            Correo:mapeo.rows[0].CorreoUsuario,
            FechaRegistro:mapeo.rows[0].FechaRegistro,
            FechaActualizar:mapeo.rows[0].FechaActualizar

        }

    }catch(e){

        json_respuesta = {

            error:true,
            mensaje:( await mensaje(0))
        }


    }

    return json_respuesta;    

}

export const registrar = async(token, id) =>{

    const consulta = `INSERT INTO public.
    "recuperarCuenta"("IdUsuario", "tokenRecuperar", "TiempoLimite", "estadoRecuperar", "fechaRegistro", "fechaActualizar")
        VALUES ($1, $2, $3, $4, $5, $6)`;

    const limites = await tiempoLimites();    

    const hoy = await DiaHoy();

    const datos = [id,token,limites,1,hoy,null];

    const registrar = await db.query(consulta, datos);

    if(registrar.rowCount === 1){

        json_respuesta = {

            url:true

        }

    }

    return json_respuesta;

}

export const buscarUrl = async(url) =>{

    const consulta = 'SELECT * FROM public."recuperarCuenta" WHERE "tokenRecuperar" = $1';

    const dato = [url];

    const mapeo = await db.query(consulta, dato);

        json_respuesta = {

            error:false,
            mensaje:'ningun error',
            IDToken:mapeo.rows[0].idRecuperar,
            ids:mapeo.rows[0].IdUsuario,
            toke:mapeo.rows[0].tokenRecuperar,
            tiempoLimite:mapeo.rows[0].TiempoLimite,
            estadoRecuperar:mapeo.rows[0].estadoRecuperar,
            fechaRegistro:mapeo.rows[0].fechaRegistro,
            fechaActualziar:mapeo.rows[0].fechaActualizar
    
    }

    return json_respuesta;
}

export const tablaRecuperar = async(id)=>{

    const consulta = 'SELECT * FROM public."recuperarCuenta" WHERE "IdUsuario" = $1';

    const dato = [id];

    const mapeo = await db.query(consulta, dato);

    if(mapeo.rowCount >= 1){

        const logn = mapeo.rows.length;

        json_respuesta = {

           nextProcess:false,
           recuperar:mapeo.rows[logn-1]
   
       }

    }else{

        json_respuesta = {

            nextProcess:true,
            mensaje:'no existe regristo de solicitud'

        }

    }



    return json_respuesta;

}

export const modEstatus = async(id,estatus) =>{

    const idUser = await id;

    const estado = await estatus;

    try{

        const consulta = 'UPDATE public."recuperarCuenta" SET  "estadoRecuperar" = $1, "fechaActualizar" = $2 WHERE "idRecuperar" = $3;';
    
        const datos = [estado,(await tiempos.fechaHoy()), idUser];
    
        const update = await db.query(consulta, datos);


    }catch(e){

        console.log(e);

    }

    

}

export const modContrasenia = async(contra, id) =>{

    try{

        const consulta = 'UPDATE public."usuariosClicko" SET contrasenia=$1, "FechaActualizar" = $2 WHERE "IdUsuario" = $3 ';

        const datos = [contra, (await fechaHoy()) ,id];
    
        const actualizar = await db.query(consulta, datos);

    }catch(e){

        console.log(e);

    }

}

export const DiaHoy = async() =>{

    const fecha = time.getDate()+"/"+time.getMonth()+"/"+time.getFullYear();

    return fecha;

}

export const tiempoLimites = async() =>{

    const limite = 60/(1000/(time.getTime()+(1000*(60*5))));

    return limite;

}