//fichero con funciones para configuraciones 
//de recuperación de cuenta

//variable globales
var json_resp = {};

//requerimos fichero para enviar correos
import {correoRecuperarCuenta} from '../../../../util/correos/index.mjs';

//requrimos fichero de control de tiempo
import {limitesTiempo} from '../../../../util/tiempo/index.mjs';

//requerimos fichero para db
import {modEstatus,registrar,buscarUrl,usuarioCorreo,modContrasenia} from '../db/index.mjs';

//requerimos ficheroc con mensajes de erroes
//const errroes = require('../error/index.mjs')

export const nuevoToken = async () =>{

    const abcRequerido = "123abcd456efg789hij0zyxwvutsRQPKLMNO";

    var token = "";

    for(let i = 0;i<24;i++){

        token = token+abcRequerido[Math.floor(Math.random() * (abcRequerido.length - 0))];

    }
    
    return token;

}

export const urlRecuperar = async(token) =>{

    const url = 'http://localhost:8080/recuperar-cuenta/'+token;

    return url;

}

//construir url
//configuracion
export const solidRecuperar = async(usuario) =>{

    const myDatos = await usuario;

    //datos de la ultima solicitd
    //@parametros ID
    const datoRecuperar = await db.tablaRecuperar(myDatos.ids);

    if(datoRecuperar.nextProcess === true){
        
        json_resp = {

            respuestaSolid:(await registrarRecuperacion(myDatos)),
            mensaje:'Se te ha enviado un mensaje a tu correo electronico, para recuperar tu cuenta'

        }

    }else{

        const limites = await validarTiempo(datoRecuperar.recuperar.TiempoLimite, datoRecuperar.recuperar.idRecuperar);

        if(limites === true){

            json_resp = {
     
                respuestaSolid:(await registrarRecuperacion(myDatos)),
                mensaje:'Se te ha enviado un mensaje a tu correo electronico, para recuperar tu cuenta'
    
            }

        }else{

            json_resp = {
     
                respuestaSolid:false,
                mensaje:(await errroes.mensaje(1))

            }

        }

    }

    return json_resp;

}

export const registrarRecuperacion = async(myDatos) =>{

    const usuario = await myDatos;

    const motivo = 'recuperar cuenta';

    const token = await nuevoToken();

    const url = await urlRecuperar(token);

    registrar(token,usuario.ids);

    correoRecupear(usuario.Correo,usuario.nombres,url,motivo);

    return true;

}

export const validarTiempo = async(tiempoLimite, id) =>{

    const limite = await tiempoLimite;

    const tiempoActual = await limitesTiempo();

    const idUser = await id;

    if(tiempoActual > limite){

        modEstatus(idUser,2);

        return true

    }else{

        return false

    }

}
export const validarEstado = async(estatus) =>{

    const estado = await estatus;

    if(estado > 1){
    
        return true;

    }else{

        return false;

    }


}

export const correoRecupear = async(correo, nombre, url, motivo)=>{

    enviarCorreo.correoRecuperarCuenta(correo,nombre,url,motivo);

}
export const valiUrls = async(url)=>{

    const ulr = await url;

    const tablaRecuperar = await buscarUrl(ulr);

    const tiempoLimite = await validarTiempo(tablaRecuperar.TiempoLimite);

    if(tiempoLimite === false){

        const NuevatablaRecuperar = await buscarUrl(ulr);

        const respVal = await validarEstado(NuevatablaRecuperar.estadoRecuperar);

        if(respVal === false){

            json_resp = {
    
                valUrlError:false,
                mensaje:'Sin errores',
                idUsuarios:tablaRecuperar.ids,
                idToken:tablaRecuperar.IDToken
            }
        }else{
    
            json_resp = {
    
                valUrlError:true,
                mensaje:(await errroes.mensaje(2))
    
            }
        }

    }else{

        json_resp = {
    
            valUrlError:true,
            mensaje:(await errroes.mensaje(2))

        }

    }

    return json_resp;

}
export const mapeoUsuario = async(correo)=>{

    const respUsuario = await usuarioCorreo(correo);

    return respUsuario;

}

export const modificarContra = async(ps,id, idUr)=>{

    const contra = await ps;

    const idUser = await id;

    const idUrl = await idUr;

    modContrasenia(contra, idUser);

    modEstatus(idUrl,2);

    json_resp = {

        actualizar:true,
        mensaje:'Se a actualizado correctamente, ya puedes ingresar con tu nueva contraseña'

    }

    return json_resp;

}
