//fichero para funciones
//para recuperar cuenta

//variable global
var json_resp = {};

//requerimos constantes 
//import {dominio} from '../../../constantes';
//requerimos fichero de db de usuario
//const dbuser = require('../../../db/model/usarios');
//requerimos fichero con configuracion
import {mapeoUsuario,solidRecuperar,valiUrls,modificarContra} from './config/index.mjs';

export const cuenta = async (correo) =>{
    const motivo = 'recuperar cuenta';
    const usuario = await mapeoUsuario(correo);
    const respUrl = await solidRecuperar(usuario);
    return respUrl;
}

export const valirUrl = async (url)=>{
    const resp = await valiUrls(url);
    return resp;
}

export const modContra = async (ps,id,idUr) =>{
    const resp = await modificarContra(ps,id, idUr);
    return resp;
}
