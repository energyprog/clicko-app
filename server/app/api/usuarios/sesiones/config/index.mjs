//fichero para realizar actividades externar
//a funciones dentro de las constantes de sesiones

//importamos fichero de errores
import {errorSesion} from '../error/index.mjs'

import {queryRegistro,querUsuario} from '../../registro/db/index.mjs';

import {registroSesion} from '../db/index.mjs';

var json_resp = {};

var errores;

//funcion para volver a validar datos
//con paramaetro con los datos recibidos 
export const validarDatos = async(datos) =>{
        const correo = datos.correo;
        const contrasenia = datos.contrasenia;
        if(correo === "" || contrasenia === ""){
               errores = "ErrorS1";
               const err = await errorSesion(errores);
               json_resp = {
                'selloError':err
               }
        }else{
                json_resp = {
                        'selloError':[false,'no existe errores']
                };
        }
        return json_resp;
}

export const activarGoogle = async(datos) =>{
        const usuario = await querUsuario(datos.correo);
        if(usuario.error === true){
                const registroUsuario = await queryRegistro(datos);
                const nuevaSesion = await activarSesion(datos.correo,2);
               json_resp = {
                sesion:nuevaSesion,
                usuario:{
                        id:nuevaSesion.ids
                }
               }
        }else{
                const nuevaSesion = await activarSesion(datos.correo,2);
                    json_resp = {
                sesion:nuevaSesion,
                usuarios:{     
                        id:usuario.ids
                }
               }
        }
        return json_resp;
}

export const activarSesion = async(correo,metodo) =>{
        const nuevoUsuario = await querUsuario((await correo));
        const respActivar = await registroSesion(nuevoUsuario.ids,metodo);

        return respActivar;
}

export const valSesion = async(correo, pass) =>{
        const mapeo = await querUsuario(correo);
        if(mapeo.error === false){
                if(((mapeo.Contrasenia).split(' '))[0] === pass){
                        activarSesion(correo,1)
                        json_resp = {
                                errorSesion:false,
                                sesion:true,
                                nombre:((mapeo.nombres).split(' '))[0],
                                id:mapeo.ids,
                                mensaje:'sin error',
                                sesion:'start'
                        }
                }else{
                        json_resp = {
                                errorSesion:true,
                                sesion:false,
                                mensaje:'correo y/o contraseña incorrectos'
                        }
                }
        }else{
                json_resp = {
                        errorSesion:true,
                        sesion:false,
                        mensaje:'correo y/o contraseña incorrectos'
                }
        }

        return json_resp;
}