//fichero para realizar consultas
//fichero con todas las consulta para la base de datos
//base de datos usada con PostgreSQL

//requirmos libreria para encriptar datos
//const bcrypt = require('bcrypt');

//requirimos la libreria para usar la base de datos
import { connectdb } from '../../../../db/connectdb.mjs';

const db = connectdb();

//requirimos fichero de control de tiempo
import {fechaHoy} from '../../../../util/tiempo/index.mjs';
 
//variable globar
let json_datos = {};


//funcion para buscar el usuario
//con parametros (contrasenia y corre) en un arreglo

//@parametros Array
//@parametros correo y contraseña
export const registroSesion = async(id,metodo) =>{

    const consulta = 'INSERT INTO public."sesionClicko"("IdUsuario", "TipoUsuario", "EstadoSesion", "FechaRegistro", "FechaActualizar", "metodoSesion") VALUES ($1, $2, $3, $4, $5, $6)';
    const datos = [
        (await id),
        null,
        1,
        (await fechaHoy()),
        null,
        (await metodo)
    ];
    try{
        const registro = db.query(consulta,datos);
        json_datos = {
            error:false,
            sesion:"start",
            mensaje:''
        }
    }catch(e){
        console.log(e);
        json_datos = {
            error:true,
            sesion:'off',
            mensaje:'error en registrar sesion'
        }
    }
    return json_datos;
}
