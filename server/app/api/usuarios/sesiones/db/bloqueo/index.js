//fichero para hacer consultas a la base de datos
//espficiamente de una tarea
//para buscar usuarios bloquados

//importamos funciones en fichero const
const mjs = require('./const-bloq-mjs');

//variables globales
var json_datos = {};

//funcion para validar 
//usuarios suspendidos
const valBloq =async (datos, db) =>{

    const id = [datos.idUsuario,2];

    const consulta = 'SELECT type_status, fecha_libre_status, hora_libre_status FROM public.status_usuarios WHERE "idusuarios" = $1 AND tipo_accion = $2';

    const resp = await db.query(consulta,id);

    const longStatus = resp.rowCount;

    if(longStatus < 1 ){

        json_datos = [false,'tu cuenta esta al 100%'];

        console.log('funcion bloqueo')

    }else{

        const tipoStatus = resp.rows.type_status;

        const respB = mjs.bloqmjs(tipoStatus);

        json_datos = [respB];

    }

    return json_datos;

}




module.exports = {

    valBloq

}