//fichero para realizar consultas 
//En este caso: para sesiones de usuarios

//requirimos fichero de config
import {valSesion,activarGoogle} from './config/index.mjs'

var json_datos = {};

//funcion para iniciar sesión
//esta funcion nos puede ayudar para validar
//sesiones guardadas de manera local
//de igual manera activa o declinar la sesion
export const actSesion = async(datos) =>{
    console.log(datos.emial);
    const resp = await valSesion(datos.emial,datos.contrasenia);
    return resp;
} 

export const sesionGoogle = async(datos) =>{
    const usuario = await datos;
    const activarSesion  = await activarGoogle(usuario);
    return activarSesion;
}

